import { createBottomTabNavigator } from 'react-navigation';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Tab1 from './component/tab1';
import Tab2 from './component/tab2';
import Tab3 from './component/tab3';

export const Router = createBottomTabNavigator({
    Tab1: Tab1,
    Tab2: Tab2,
    Tab3: Tab3,
},
{
    navigationOptions: ({ navigation }) => ({
        tabBarIcon: ({ focused, horizontal, tintColor }) => {
          const { routeName } = navigation.state;
          let iconName;
          if (routeName === 'Tab1') {
            iconName = `ios-information-circle${focused ? '' : '-outline'}`;
          } else if (routeName === 'Tab2') {
            iconName = `ios-options${focused ? '' : '-outline'}`;
          } else if (routeName === 'Tab3') {
            iconName = `ios-options${focused ? '' : '-outline'}`;
          }
  
          // You can return any component that you like here! We usually use an
          // icon component from react-native-vector-icons
          return <Ionicons name={iconName} size={horizontal ? 20 : 25} color={tintColor} />;
        },
      }),
      tabBarOptions: {
        activeTintColor: 'tomato',
        inactiveTintColor: 'gray',
      },
})